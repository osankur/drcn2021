# drcn2021
In our experiment of verification of SDN platform, we used the SPIN software to code the system behavior and verifying the two properties Isolation and Data reachability properties.


In order to run the experiment, machine should have installed SPIN software, a open source well documented classical model checking software. 

In order to run a model in a simulation mode type 

spin filename.pml
which runs for one possible simulation of the model coded in the promela language ( process meta language ) 

for running the code in verification mode (I.e check for the all possible scenario of the created model)

type the following in the terminal

spin -a  filename.pml  # which creates a C code pan.c
gcc -DVECTORSZ=1024 -o pan pan.c # compiling the c code, -DVECTORSZ one should set a larger number if the program states need more space. (Default I.e without the command -DVECTORSZ, is 1024 KB ) 
gcc -DVECTORSZ=1024 -DCOLLAPSE – o pan pan.c ( DCOLLAPSE,  In addition the compiled code use the data compression technique like hash function ) 
time ./pan ( to finally run the model in a verification code )

At the termination of the program, it shows the time the system took to complete 
and also the Memory usage. 

If there is an error it will create an file as filename.pml.trail

to see what was the error in the program, run the following command

spin -t -v -p filename.pml.

It will show the cause for the error. ( But careful to indicate the appropriate DVECTORSZ, o.w because of out of memory the program stops and it will be indicated as an error )



 Regarding the experiment of our work please read the subsequent readme file in the folder 

monolithic_mobility_case,
monolithic_verification_no_mobility,
one_domain_CR_test_mobility_safety_only ,
one_domain_CR_test_no_mobility,
one_domain_CR_three_layer_test_no_mobility,
 two_domain_CR_test_mobility_safety_only,                                       two_domain_CR_test_no_mobility,                                                      
 two_domain_CR_three_layer_test_no_mobility,
three_domain_CR_test_mobility_safety_only,
three_domain_CR_test_no_mobility,  and  
three_domain_CR_three_layer_test_no_mobility.            



Before going to see the way to run the verification by compositional reasoning method
 we will, first see the possible data plane topology for different size of SDN platform.


For one domain,  1,2,3,4, and 5, there are one,  one, two, four and five possible data plane topology.

For two domain, [1,1], [2,1], [3,1] (referred as dist1 in the folder), [2,2](refered as dist2 in the folder), and [2,3] switch distribution in each domain has one, one, two, one, and two possible data plane topology as per the constraint of our SDN platform network distributions

For three domain case, of switch distribution [1,1,1], [1,1,2], and [1,1,3] there are one, one and two 
possible data plane topology respectively.


In the following folder:  
one_domain_CR_test_mobility_safety_only ,
two_domain_CR_test_mobility_safety_only,
three_domain_CR_test_mobility_safety_only, for the mobility cases with local controller solution and verification by Compositional method (split by two management plane + controller plane in controller side and rest in the data plane)

one_domain_CR_test_no_mobility,
two_domain_CR_test_no_mobility,
three_domain_CR_test_no_mobility,  for the no-mobility cases by Compositional method (split by two management plane + controller plane in controller side and rest in the data plane referred as CR method1 in the paper )



one_domain_CR_three_layer_test_no_mobility,
two_domain_CR_three_layer_test_no_mobility,
three_domain_CR_three_layer_test_no_mobility, for the no-mobility cases by Compositional method (split by three management plane,  controller plane, and  data plane referred as CR method2 in the paper )


to run the code just type 
the following command in the terminal


time ./one_of_the_CR_script from each folder


at the end of the program check the time and for individual spin code test, check the maximum memory used and also notice that all the test, the number of error is zero.


